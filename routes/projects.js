var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var con = mysql.createConnection({
  host: 'localhost',
  database: 'Kian',
  user: 'root',
  password: '',
});

con.connect(function (err) {
  if (err) throw err;
  console.log('Connected!');
});

router.get('/', function (req, res, next) {
  con.query('SELECT * FROM Projects', function (err, result, fields) {
    let data = {};
    data.columns = [
      {
        name: 'code',
        label: 'کد',
        align: 'left',
        field: 'code',
        sortable: true,
      },
      {
        name: 'name',
        align: 'left',
        label: 'عنوان',
        field: 'name',
        sortable: true,
      },
      {
        name: 'description',
        label: 'توضیحات',
        field: 'description',
        align: 'left',
      },
    ];
    data.rows = result;

    res.json(data);
  });
});

router.post('/', (req, res) => {
  const body = req.body;
  let sql =
    "INSERT INTO Projects (`code` , `name` , `description`) VALUES ( '" +
    body.code +
    "' , '" +
    body.name +
    "' , '" +
    body.description +
    "')";

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('1 record inserted');
    res.send('Done');
  });
});

router.put('/:id', (req, res) => {
  const body = req.body;
  let sql =
    "UPDATE People SET `code` = '" +
    body.code +
    "' , `name` = '" +
    body.name +
    "' ,  `description` = '" +
    body.description +
    "' WHERE 'id'= '" +
    body.id +
    "'";

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('1 record edited');
    res.send('Done');
  });
});

router.delete('/:id', function (req, res) {
  var id = req.params.id;

  var sql = 'DELETE FROM projects WHERE id = ' + id;
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('Number of records deleted: ' + result.affectedRows);
  });

  return res.status(200);
});

module.exports = router;
