var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var con = mysql.createConnection({
  host: 'localhost',
  database: 'Kian',
  user: 'root',
  password: '',
});

con.connect(function (err) {
  if (err) throw err;
  console.log('Connected!');
});

router.get('/', function (req, res, next) {
  con.query('SELECT * FROM People', function (err, result, fields) {
    let data = {};
    data.columns = [
      {
        name: 'code',
        align: 'left',
        label: 'کد',
        field: 'code',
        sortable: true,
      },
      {
        name: 'name',
        align: 'left',
        label: 'نام',
        field: 'name',
        sortable: true,
      },
      {
        name: 'idCard',
        label: 'کد ملی',
        field: 'idCard',
        align: 'left',
        sortable: true,
      },
      { name: 'phone', label: 'شماره تلفن', align: 'left', field: 'phone' },
      { name: 'address', label: 'آدرس', align: 'left', field: 'address' },
      {
        name: 'description',
        label: 'توضیحات',
        align: 'left',
        field: 'description',
      },
    ];
    data.rows = result;

    res.json(data);
  });
});

router.post('/', (req, res) => {
  const body = req.body;
  let sql =
    "INSERT INTO People (`code` , `name` , `idCard` , `phone`  , `address` ,`description`) VALUES ( '" +
    body.code +
    "' , '" +
    body.name +
    "' , '" +
    body.idCard +
    "' , '" +
    body.phone +
    "' , '" +
    body.address +
    "' , '" +
    body.description +
    "')";

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('1 record inserted');
    res.send('Done');
  });
});

router.put('/:id', (req, res) => {
  const body = req.body;
  let sql =
    "UPDATE People SET `code` = '" +
    body.code +
    "' , `name` = '" +
    body.name +
    "' , `idCard` = '" +
    body.idCard +
    "'  , `phone` = '" +
    body.phone +
    "'  , `address` = '" +
    body.address +
    "' ,  `description` = '" +
    body.description +
    "' WHERE 'id'= '" +
    body.id +
    "'";

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('1 record edited');
    res.send('Done');
  });
});

router.delete('/:id', function (req, res) {
  var id = req.params.id;

  var sql = 'DELETE FROM People WHERE id = ' + id;
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('Number of records deleted: ' + result.affectedRows);
  });

  return res.status(200);
});

module.exports = router;
