var express = require('express');
var router = express.Router();

router.get('/materials', function (req, res, next) {
  res.render('index', { title: 'Materials' });
});

module.exports = router;
