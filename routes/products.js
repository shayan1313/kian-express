var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var con = mysql.createConnection({
  host: 'localhost',
  database: 'Kian',
  user: 'root',
  password: '',
});

con.connect(function (err) {
  if (err) throw err;
  console.log('Connected!');
});

router.get('/', function (req, res, next) {
  con.query('SELECT * FROM Products', function (err, result, fields) {
    let data = {};
    if (result.isBasic === 0) {
      data.columns = [
        {
          name: 'code',
          align: 'left',
          label: 'کد',
          field: 'code',
          sortable: true,
        },
        {
          name: 'name',
          align: 'left',
          label: 'عنوان',
          field: 'name',
          sortable: true,
        },

        {
          name: 'derivatives',
          label: 'مشتقات',
          align: 'left',
          field: 'derivatives',
        },
        {
          name: 'description',
          label: 'توضیحات',
          align: 'left',
          field: 'description',
        },
      ];
    } else {
      data.columns = [
        {
          name: 'code',
          align: 'left',
          label: 'کد',
          field: 'code',
          sortable: true,
        },
        {
          name: 'name',
          align: 'left',
          label: 'عنوان',
          field: 'name',
          sortable: true,
        },

        {
          name: 'description',
          label: 'توضیحات',
          align: 'left',
          field: 'description',
        },
      ];
    }
    result.forEach((product) => {
      if (product.derivatives) {
        let derivativesString = '';
        let dets = JSON.parse(product.derivatives);
        dets.forEach((det) => {
          let st = det.name + ' : ' + det.count + 'واحد\n';
          derivativesString = derivativesString + st;
        });
        product.derivatives = derivativesString;
      }
    });

    data.rows = result;
    res.json(data);
  });
});

router.post('/', (req, res) => {
  const body = req.body;
  let sql = '';
  console.log(102, body.isBasic);
  if (body.isBasic === 0) {
    sql =
      "INSERT INTO Products (`code` , `name` , `isBasic` , `derivatives` , `description`) VALUES ( '" +
      body.code +
      "' , '" +
      body.name +
      "' , '" +
      body.isBasic +
      "' , '" +
      body.derivatives +
      "' , '" +
      body.description +
      "')";
  } else {
    sql =
      "INSERT INTO Products (`code` , `name` , `isBasic` , `description`) VALUES ( '" +
      body.code +
      "' , '" +
      body.name +
      "' , '" +
      body.isBasic +
      "' , '" +
      body.description +
      "')";
  }

  console.log(101, sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('1 record inserted');
    res.send('Done');
  });
});

router.put('/:id', (req, res) => {
  const body = req.body;
  let sql =
    "UPDATE Products SET `code` = '" +
    body.code +
    "' , `name` = '" +
    body.name +
    "' ,  `description` = '" +
    body.description +
    "' WHERE 'id'= '" +
    body.id +
    "'";

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log(801, result);
    res.send('Done');
  });
});

router.delete('/:id', function (req, res) {
  var id = req.params.id;

  var sql = 'DELETE FROM Products WHERE id = ' + id;
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('Number of records deleted: ' + result.affectedRows);
  });

  return res.status(200);
});

module.exports = router;
