var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var con = mysql.createConnection({
  host: 'localhost',
  database: 'Kian',
  user: 'root',
  password: '',
});

con.connect(function (err) {
  if (err) throw err;
  console.log('Connected!');
});

router.get('/', function (req, res, next) {
  con.query('SELECT * FROM Inventory', function (err, result, fields) {
    let data = {};
    data.columns = [
      {
        name: 'code',
        align: 'left',
        label: 'کد',
        field: 'product_id_',
        sortable: true,
      },
      {
        name: 'name',
        align: 'left',
        label: 'عنوان',
        field: 'holder_',
        sortable: true,
      },

      {
        name: 'holder',
        label: 'نگه دارنده',
        align: 'left',
        field: 'quantity',
      },
    ];

    data.rows = result;
    res.json(data);
  });
});

// router.post('/', (req, res) => {
//   const body = req.body;
//   let sql = '';
//   console.log(102, body.isBasic);
//   if (body.isBasic === 0) {
//     sql =
//       "INSERT INTO Products (`code` , `name` , `isBasic` , `derivatives` , `description`) VALUES ( '" +
//       body.code +
//       "' , '" +
//       body.name +
//       "' , '" +
//       body.isBasic +
//       "' , '" +
//       body.derivatives +
//       "' , '" +
//       body.description +
//       "')";
//   } else {
//     sql =
//       "INSERT INTO Products (`code` , `name` , `isBasic` , `description`) VALUES ( '" +
//       body.code +
//       "' , '" +
//       body.name +
//       "' , '" +
//       body.isBasic +
//       "' , '" +
//       body.description +
//       "')";
//   }

//   console.log(101, sql);
//   con.query(sql, function (err, result) {
//     if (err) throw err;
//     console.log('1 record inserted');
//     res.send('Done');
//   });
// });

// router.put('/', (req, res) => {
//   console.log(6303, req.body);
//   const body = req.body;
//   let sql =
//     "INSERT INTO Products (`code` , `name` , `derivatives` ,`description`) VALUES ( '" +
//     body.code +
//     "' , '" +
//     body.name +
//     "' , '" +
//     body.derivatives +
//     "' , '" +
//     body.description +
//     "')";
// });

router.delete('/:id', function (req, res) {
  var id = req.params.id;

  var sql = 'DELETE FROM Products WHERE id = ' + id;
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log('Number of records deleted: ' + result.affectedRows);
  });

  return res.status(200);
});

module.exports = router;
